<!doctype html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="{{ bloginfo('charset') }}">
		<title>{{ wp_title('') }}</title>

		<script type="text/javascript">
			GLOBAL_SiteUrl = '{{ get_site_url() }}';
			GLOBAL_ThemeUrl = '{{ asset('') }}';
		</script>

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		{{ wp_head() }}

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>
	<body {{ body_class() }}>
		<header>
			<div class="navbar navbar-fixed-top" role="navigation">
				<div class="container">
					<div class="navbar-header">

						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Menu</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="<?php echo get_home_url(); ?>">
							<img src="http://placehold.it/200x50">
						</a>
					</div>

					<?php
						wp_nav_menu(
							array(
								'theme_location'    => 'main-menu',
								'depth'             => 2,
								'container'         => 'div',
								'container_class'   => 'collapse navbar-collapse',
								'container_id'      => 'bs-example-navbar-collapse-1',
								'menu_class'        => 'nav navbar-nav navbar-text navbar-right',
								'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
								'walker'            => new wp_bootstrap_navwalker()
							)
						);
					?>
				</div>
			</div>
		</header>
		<?php
			if (have_posts()): 
				while (have_posts()): the_post();
		?>

		@yield('content')

		<?php
				endwhile;
			endif;
		?>

		<footer class="clearfix">
			
		</footer>

		{{ wp_footer() }}
	</body>
</html>
