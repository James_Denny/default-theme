var gulp 		= require('gulp'),
	path 		= require('path'),
	sass 		= require('gulp-sass'),
	gutil		= require('gulp-util'),
	concat		= require('gulp-concat'),
	browserSync = require('browser-sync').create();

gulp.task('sass', function(done) {
	return gulp.src('./css/sass/styles.scss')
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(gulp.dest('./css'))
		.pipe(browserSync.stream());
	done();
});

gulp.task('combine:css', function (done) {
	return gulp.src('./css/libs/*.css')
		.pipe(concat('./css/libs.css'))
		.pipe(gulp.dest('./'));
	done();
});

gulp.task('combine:app', function (done) {
	return gulp.src(['./css/libs.css', './css/styles.css'])
		.pipe(concat('./css/app.css'))
		.pipe(gulp.dest('./'));
	done();
});

gulp.task('combine:js', function (done) {
	return gulp.src(['./js/libs/jquery*.js', './js/libs/*'])
		.pipe(concat('libs.js'))
		.pipe(gulp.dest('./js/'));
	done();
});

gulp.task('serve', gulp.series('sass', 'combine:css', 'combine:app', function () {

	var dir = path.dirname(path.resolve('../', '../')).split('\\');
	var folder = dir[dir.length - 1];

	browserSync.init({
		proxy: "yaya.dev/" + folder
	});

	// Watch for libraries
	gulp.watch('./css/libs/*.css', gulp.series('combine:css'));
	gulp.watch('./js/libs/*.js', gulp.series('combine:js'));

	// Watch SCSS changes.
	gulp.watch(['./css/sass/*.scss', './css/sass/**/*'], gulp.series('sass'));

	// If any SCSS or Libraries change combine files into the one file.
	gulp.watch(['./css/libs.css', './css/styles.css'], gulp.series('combine:app'));

	// Watch all static files, which require a page reload.
	gulp.watch(['./*.php', './**/*.php', './js/*', './includes/*', './css/*']).on('change', browserSync.reload);
}));

gulp.task('default', gulp.series('serve'));