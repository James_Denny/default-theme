<?php

// ENSURE JETPACK IS INSTALLED

// Register Custom Navigation Walker
require_once('includes/wp_bootstrap_navwalker.php');

function site_scripts() {

	//wp_enqueue_style('libs', get_template_directory_uri() . '/css/libs.css');
	//wp_enqueue_style('styles', get_template_directory_uri() . '/css/styles.css');
	wp_enqueue_style('app', get_template_directory_uri() . '/css/app.css');

	wp_enqueue_style('style', get_stylesheet_uri());

	wp_deregister_script('jquery');
	wp_deregister_script('wp-embed');

	wp_enqueue_script('libs', get_template_directory_uri() . '/js/libs.js');
	//wp_enqueue_script('script', get_template_directory_uri() . '/js/script.js');
	//wp_enqueue_script('app', get_template_directory_uri() . '/css/app.js');


}

remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

function site_menus() {
	register_nav_menu('main-menu', __( 'Main Menu' ));
}

add_action('init', 'site_menus');

add_action('wp_enqueue_scripts', 'site_scripts');

add_theme_support( 'post-thumbnails' ); 

add_filter('show_admin_bar', '__return_false');

function asset($asset) {
	?><?php echo get_template_directory_uri() . '/' . $asset ?><?php
}

function get_the_content_with_formatting ($more_link_text = '(more...)', $stripteaser = 0, $more_file = '') {
	$content = get_the_content($more_link_text, $stripteaser, $more_file);
	$content = apply_filters('the_content', $content);
	$content = str_replace(']]>', ']]&gt;', $content);
	return $content;
}

function trim_text($input, $length) {
	if (strlen($input) <= $length) {
		return $input;
	}

	$last_space = strrpos(substr($input, 0, $length), ' ');
	$trimmed_text = substr($input, 0, $last_space);

	return $trimmed_text;
}

function srcset($endPoint, $x1, $x2, $class = "", $alt = "") {
	?>
		<img alt="<?php echo $alt ?>" class="<?php echo $class ?>"  src="<?php bloginfo('template_directory'); echo $endPoint . $x1; ?>" 
			srcset="<?php bloginfo('template_directory'); echo $endPoint . $x1; ?> 1x, 
			<?php bloginfo('template_directory'); echo $endPoint . $x2; ?> 2x">
	<?php
}

function debug($v) {
	echo '<pre>';
	var_dump($v);
	echo '</pre>';
}

function get_parent_id($slug, $post_id) {

	$find = new WP_Query(array(
		'p' => $post_id
	));

	while ($find->have_posts()): $find->the_post();
		$id = get_post_meta($post_id, '_wpcf_belongs_'.$slug.'_id', true);
	endwhile;

	return $id;
	
}

function sort_by_menu_order($a, $b) {
	if ($a->menu_order == $b->menu_order) {
		return 0;
	}
	return ($a->menu_order < $b->menu_order) ? -1 : 1;
}

function wpdocs_custom_excerpt_length( $length ) {
    return 40;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

function wpdocs_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );